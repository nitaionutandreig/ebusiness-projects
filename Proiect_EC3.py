import numpy as np # modul data science
import pandas as pd # modul de data analysis
from sklearn.linear_model import LinearRegression # importam clasa de regresie liniara
from sklearn.cross_validation import train_test_split # metoda pentru machine learning
from sklearn.datasets import load_boston # date de test integrate in scikit-learn
# explicatie pentru continut load_boston: http://occam.olin.edu/sites/default/files/DataScienceMaterials/machine_learning_lecture_1/Machine%20Learning%20Lecture%201.html
from sklearn.cluster import KMeans # algoritm KMeans

boston = load_boston()


x = pd.DataFrame(boston.data, columns=boston.feature_names)

print (x.describe())

y = pd.DataFrame(boston.target)

print (y.describe())

# Incercam  cu metoda train_test_split a sklearn pentru a verifica acuratetea predictiei
# Cream obiectul Linear Regression
reg = LinearRegression()

# Facem un split la date in date de training si date de test
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=4)

# Incercam sa facem fit la date
reg.fit(x_train, y_train)

# Coeficientul in decizia functiei
coef = reg.coef_

print ('\nCoeficientul este: {}\n'.format(coef))

print ('Panta aferenta este: {}\n'.format(coef[0][0]))

# Recuperam si intersectia cu OY sau y_Intercept (b)
intercept = reg.intercept_[0]

print ('Intercept este: {}\n'.format(intercept))

# Incercam sa facem o predictie
predictions = reg.predict(x_test)

print ('Predictiile pentru date:')
print (predictions)

# Calculam eroarea medie
r_squared = np.mean((reg.predict(x_test) - y_test)**2).values[0]

print ('\nEroarea medie calculata este: {}\n'.format(r_squared))

# Incercam sa facem clustering la date, o sa presupunem ca ar fi 4 clustere
kmeans = KMeans(n_clusters=4)
kmeans.fit(x)
centroids = kmeans.cluster_centers_

print ('Centroizii aferenti sunt: {}'.format(centroids))