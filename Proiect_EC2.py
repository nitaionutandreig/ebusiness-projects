from statistics import mean # import functia medie aritmetica
import matplotlib.pyplot as plt # modul grafice
from matplotlib import style
import quandl # modul care recupereaza date de pe https://www.quandl.com
style.use('ggplot')

date_quandl = quandl.get("GPP/CFP_ROU", authtoken="29shdevMPMKizVwysUzx")

pret_benzina = date_quandl['Gasoline Price']
pret_diesel =  date_quandl['Diesel Price']
data = date_quandl.index.values

print ('Analizam datele')
print ('Benzina -> Pret maxim: {}, pret mediu: {:.3}, pret minim: {}'.format(max(pret_benzina), mean(pret_benzina), min(pret_benzina)))
print ('Diesel -> Pret maxim: {}, pret mediu: {:.3}, pret minim: {}'.format(max(pret_diesel), mean(pret_diesel), min(pret_diesel)))

# Desenam un grafic cu datele actuale
plt.figure(1, figsize = (10, 5))
plt.title('Evolutie pret combustibil')
plt.xlabel('Data')
plt.ylabel('Pret combustibil')
plt.plot(data, pret_benzina, color='b', label='pret benzina')
plt.plot(data, pret_diesel, color='r', label='pret motorina')
plt.legend(loc=4)
plt.show()

# Formula este b = y - mx
# y - cat de sus se ajunge pe axa OY
# x - cat de departe se ajunge pe axa OX
# m - panta sau unghiul liniei
# b - intersectia cu axa OY

# Algoritm: https://pythonprogramming.net/how-to-program-best-fit-line-machine-learning-tutorial/
def regresie_liniara(pret_benzina,pret_diesel):
    m = (((mean(pret_benzina)*mean(pret_diesel)) - mean(pret_benzina*pret_diesel)) /
         ((mean(pret_benzina)*mean(pret_benzina)) - mean(pret_benzina*pret_benzina)))
    b = mean(pret_diesel) - m*mean(pret_benzina)
    return m, b

# aflam panta si intersectia
m, b = regresie_liniara(pret_benzina,pret_diesel)
print('m este: {} iar b este: {}'.format(m,b))

# calculam linia de regresie
regression_line = [(m * x) + b for x in pret_benzina]

# Desenam graficul de regresie liniara
plt.figure(2, figsize = (10, 4))
plt.title('Regresie liniara')
plt.xlabel('Pret Benzina')
plt.ylabel('Pret Motorina')
plt.scatter(pret_benzina,pret_diesel,color='b', label='date')
plt.plot(pret_benzina, regression_line, label='Linie regresie')
plt.legend(loc=4)
plt.show()

# Acum incercam sa prezicem cat va deveni pretul
predict_x = 5.5
predict_y = (m * predict_x) + b
            
print ('Prezicem ca pretul benzinei ar creste la: {}'.format(predict_x))
print ('Reiese ca pretul motorinei ar atinge: {}'.format(predict_y))
            
# Desenam graficul cu predictia noastra
plt.figure(3, figsize=(15, 4))
plt.title('Predictie pret')
plt.xlabel('Pret Benzina')
plt.ylabel('Pret Motorina')
plt.scatter(pret_benzina, pret_diesel, color='b', label='date')
plt.plot(pret_benzina, regression_line, label='Linie regresie')
plt.scatter(predict_x, predict_y, label='predictie', color='g')
plt.legend(loc=4)
plt.show()

