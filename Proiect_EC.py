import requests #recupereaza content
from bs4 import BeautifulSoup # parseaza content
import pandas as pd # modul data analysis pentru dataframe
import matplotlib.pyplot as plt # modul grafice
from sklearn.linear_model import LinearRegression as LinReg 
from sklearn.cluster import KMeans # clustering, algoritm kmeans

#Top 250 movies IMDB
URL = 'http://www.imdb.com/chart/top?ref_=nv_mv_250_6' 
# muting pandas warnings for modifying a dataframe slice
pd.options.mode.chained_assignment = None
# common used matplotlib style, documentation: http://ggplot.yhathq.com/
plt.style.use('ggplot')

def get_content(URL):
    """ This function takes a URL as a parameter and returns all the rows of 
    a html table that can be iterated over to recover values."""
    result = requests.get(URL) # scrape the website URL
    c = result.content # recover only the content
    soup = BeautifulSoup(c, "lxml") # create a soup beautiful soup object from the html content
    table = soup.find('table', attrs={'class':'chart full-width'}) # find the table that has our data
    table_body = table.find('tbody') # we only need the html body attribute of the table
    rows = table_body.find_all('tr') # we keep the tr element
    return rows #we return the rows that we can then iterate over

def build_dataframe(rows):
    """ This function takes table rows as a parameter and builds a dataframe
    from the selected elements. It returns a dataframe."""
    df = pd.DataFrame(columns=['Rank', 'Title', 'Rating', 'Year', 'Director', 'LeadingRole', 'SupportingRole'])
    for row in rows:
        movie = row.find('td', attrs={'class':'titleColumn'}).text.strip().split('\n')
        Rating = row.find('td', attrs={'class':'ratingColumn imdbRating'}).text.strip()
        cast = row.find('td', attrs={'class':'titleColumn'})
        for elem in cast.find_all('a'):
            Director = (elem['title'].split(','))[0].split(' (')[0]
            LeadingRole = (elem['title'].split(','))[1].strip()
            SupportingRole = (elem['title'].split(','))[2].strip()
        Rank = movie[0].split('.')[0]
        Title = movie[1].strip()
        Year = movie[2][1:5]
        df.loc[len(df)] = [Rank, Title, Rating, Year, Director, LeadingRole, SupportingRole]
    return df

def order_by_freq(dataframe):
    """ This function takes in a dataframe, iterates through its columns and outputs 
    the element with the most occurences from every column. """
    for column in dataframe.columns:
        if column not in ['Rank', 'Title']:
            df = dataframe[column].value_counts()
            print ('Best {} is {} with {} occurences.'.format(column, df.index[0], df[0]))
            
def average_Rating_by_Year(dataframe):
    """ This function takes in a dataframe as a parameter and calculates the
    average Rating by Year. It returns a dataframe with average Rating grouped by Year."""
    df_avg_r_by_y = dataframe[['Rating', 'Year']]
    df_avg_r_by_y['Year'] = pd.DatetimeIndex(df_avg_r_by_y['Year']).year
    df_avg_r_by_y['Rating'] = df_avg_r_by_y['Rating'].astype(float)
    grouped = df_avg_r_by_y.groupby([df_avg_r_by_y['Year']]).mean()
    return grouped

def calc_linear_reg(dataframe):
    """ This function takes in a dataframe for which it can output the accuracy
    and the x, y, y_preds variables needed to plot the linear regression model. 
    Algorithm source: https://github.com/athenakan/cs50seminar """
    x = dataframe.index.values.reshape(-1, 1)
    y = dataframe['Rating'].values
    reg = LinReg()
    reg.fit(x, y)
    y_preds = reg.predict(x)
    return x, y, y_preds

def plot_linear_reg(*args):
    """ This function takes in x, y and y_preds and plots a graph that shows average
    Rating grouped per Year and the linera regression model. We are trying to determine
    if movie Rating tends to increase with every passing Year. """
    plt.figure(1, figsize = (10, 8))
    plt.title('Linear Regression model for movie average Rating by Year')
    plt.xlabel('Year')
    plt.ylabel('Rating')
    x, y, y_preds = args[0]
    x = x.tolist()
    plt.scatter(x=x, y=y_preds)
    plt.scatter(x=x, y=y, c="r")

def clustering_data(dataframe):
    """ This function takes in a 2 column dataframe and generates a two cluster map. 
    Algorithm source: https://pythonprogramming.net/flat-clustering-machine-learning-python-scikit-learn/"""
    plt.figure(2, figsize = (10, 8))
    plt.title('Clustering model with 2 clusters.')
    plt.xlabel('Year')
    plt.ylabel('Rating')
    kmeans = KMeans(n_clusters=2)
    kmeans.fit(dataframe)
    centroids = kmeans.cluster_centers_
    labels = kmeans.labels_
    colors = ["b.","r."] #b. = blue, r. = red
    for i in range(len(dataframe)):
        plt.plot(dataframe.loc[i][0], dataframe.loc[i][1], colors[labels[i]], markersize = 10)   
    plt.scatter(centroids[:, 0],centroids[:, 1], marker = "x", color='g', s=150, linewidths = 5, zorder = 10)
    plt.show()

def main():
    rows = get_content(URL)
    initial_df = build_dataframe(rows)
    order_by_freq(initial_df)
    df = average_Rating_by_Year(initial_df)
    plot_linear_reg(calc_linear_reg(df))
    clustering_data(df.reset_index())


if __name__ == '__main__':
    main()